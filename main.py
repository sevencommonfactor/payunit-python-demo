import payUnit
from flask import Flask,render_template,request
  
# Flask constructor takes the name of  
# current module (__name__) as argument. 

app = Flask(__name__) 

payment = payUnit.payUnit({
    
    "user_api":'payunit_lu5jztOoX',
    "password_api":'44fd2838-2899-4f77-99fa-659a538aaccf',
    "api_key":'9f512f681bfa6d9c3c4ae0b6d3465cb494092c81',
    "return_url": "http://127.0.0.1:5000/thanks",
    "notify_url":"",
    "mode": "live",
})


@app.route('/', methods=['GET','POST']) 
def hello_world(): 
    if(request.method == "POST"):
        amount = request.form['amount']
        result = payment.makePayment(request.form['amount'])
        print(result)
    return render_template('index.html')

@app.route('/thanks', methods = ['GET'])
def welcome():
    return render_template('thanks.html')

@app.route('/notify', methods = ['POST'])
def notify():
    if(request.method == "POST"):
        print('******** headers **********')
        print(request.headers)
        print('******** body **********')
        print(request.data)
        print('A post request format arrived')
        print(request.data)
    return render_template('thanks.html')


# main driver function 
if __name__ == '__main__': 
    app.run() 
